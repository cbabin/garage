<?php
Class Camion extends Vehicule {
    private $_tonnage;
    private $_remorquelongueur;

    public function tonnage(){
        return $this->_tonnage;
    }
    public function remorquelongueur(){
        return $this->_remorquelongueur;
    }
    public function settonnage(int $tonnage){
        $this->_tonnage = $tonnage;
    }
    public function setremorquelongueur($remorquelongueur){
         $this->_remorquelongueur = $remorquelongueur;
    }
    public function displayCaracteristics(){
        echo "Camion détecté de marque " . $this->marque() . " modèle " . $this->modele() . ". <br/>";
        echo "Avec une remorque de " . $this->remorquelongueur() ." mètres et un tonnage total de " . $this->tonnage() ." tonnes. </br>";
    }
}
?>