<?php
include('Vehicule.php');
include('Garage.php');
include('bootstrap.php');
include('function.php');
$garage = new Garage;

foreach ($readcsv as $ligne) {
    $marque = $ligne[0];
    $modele = $ligne[1];
    $roues = $ligne[2];
    $portes = $ligne[3];
    $couleur = $ligne[4];
    $immatriculation = $ligne[5];
    $type = $ligne[6];
    $tonnage = $ligne[7];
    $remorquelongueur = $ligne[8];
    if (!empty($ligne[6])) {
        echo "<div class='center global voiture'>";
        if ($ligne[6] == "Voiture") {
            $ligne[6] = new $type($portes, $roues, $immatriculation, $couleur, $modele, $marque);
            $ligne[6]->displayCaracteristics();
            $garage->expertise($ligne[6]);
        } else {
            $ligne[6] = new $type($portes, $roues, $immatriculation, $couleur, $modele, $marque);
            $ligne[6]->setremorquelongueur($remorquelongueur);
            $ligne[6]->settonnage($tonnage);
            $ligne[6]->displayCaracteristics();
            $garage->expertise($ligne[6]);
        }
        echo "</div>";
    }
}
?>
<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport">
    <title>Les Voitures</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="css/style.css">
</head>
<body class="secondepage">
<div class="button center">
    <button class="btn btn-outline-primary"><a href="formulaire.php" class="link">Retourner au formulaire</a></button>
    <br>
    <button class="btn btn-outline-primary"><a href="index.php" class="link">Retourner à la page d'acceuil</a></button>
</div>
</body>
</html>