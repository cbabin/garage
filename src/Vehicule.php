<?php
Class Vehicule {
    private $_nombrederoues;
    private $_nombredeportes;
    private $_immatriculation;
    private $_couleur;
    private $_modele;
    private $_marque;
    private $_controletechnique;
    private $_moteurstatut;

    public function __construct(
        int $nombredeportes,
        int $nombrederoues,
        string $immatriculation,
        string $couleur,
        string $modele,
        string $marque)
    {   $this->setnombredeportes($nombredeportes);
        $this->setnombrederoues($nombrederoues);
        $this->setimmatriculation($immatriculation);
        $this->setcouleur($couleur);
        $this->setmodele($modele);
        $this->setmarque($marque);
    }
    //Les Getters
    public function couleur(){
        return $this->_couleur;
    }
    public function modele(){
        return $this->_modele;
    }
    public function marque(){
        return $this->_marque;
    }
    public function nombrederoues(){
        return $this->_nombrederoues;
    }
    public function nombredeportes(){
        return $this->_nombredeportes;
    }
    public function immatriculation(){
        return $this->_immatriculation;
    }
    public function controletechnique(){
        return $this->_controletechnique;
    }
    public function moteurstatut(){
        return $this->_moteurstatut;
    }
    //Les Setters
    public function setcouleur(string $couleur){
        $this->_couleur = $couleur;
    }
    public function setnombrederoues(int $nombrederoues){
        $this->_nombrederoues = $nombrederoues;
    }
    public function setimmatriculation(string $immatriculation){
        $this->_immatriculation = $immatriculation;
    }
    public function setmarque(string $marque){
        $this->_marque = $marque;
    }
    public function setmodele(string $modele){
        $this->_modele = $modele;
    }
    public function setnombredeportes(int $nombredeportes){
        $this->_nombredeportes = $nombredeportes;
    }
    public function setcontroletechnique(string $controletechnique){
        $this->_controletechnique = $controletechnique;
    }
    public function setmoteurstatut (bool $moteurstatut){
        $this->_moteurstatut = $moteurstatut;
    }
    //Les Etats
    public function demarrer(){
        $this->setmoteurstatut(true);
        echo "la voiture démarre, broum broum <br/>";
        return $this;
    }
    public function arreter(){
        $this->setmoteurstatut(false);
        echo "la voiture s'arrête, ouf <br/>";
        return $this;
    }
    public function avancer(){
        echo "la voiture avance <br/>";
        return $this;
    }
    public function freiner(){
        echo "la voiture freine <br/>";
        return $this;
    }
}
?>