<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="./css/style.css">
</head>
<body class="index">
<div class="center global">
    <h1>Bienvenue sur votre centre d'expertise automobile Margoulin</h1>
    <div class="button">
    <button class="btn btn-outline-primary"><a href="formulaire.php" class="link">Ajouter mon véhicule</a></button>
        <br>
        <button class="btn btn-outline-primary"><a href="secondepage.php" class="link">Aller dans le centre d'expertise</a></button>
    </div>
</div>
</body>
</html>