<?php
Class Voiture Extends Vehicule {
    public function displayCaracteristics(){
        echo "Voiture de marque " . $this->marque() . " modèle: " . $this->modele() . " avec " . $this->nombredeportes() . " portes .<br/> ";
    }
}