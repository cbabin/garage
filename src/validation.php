<?php
?>
<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <title>validation</title>
    <link rel="stylesheet" href="./css/style.css">
</head>
<body class="validation">
<div class="global white center">
<h1>validation</h1>
<h4>Affichage de $_FILES</h4>
<table border=1>
<thead>
<tr>
    <th>Champs</th><th>Valeur</th><th>Est Alpha ?</th><th>Est numérique ?</th>
    <th>Email ? </th><th>Int ? </th><th>Float ?</th><th>Mail autorisé ?</th>
</tr>
</thead>
<?php
$currentkey = $_POST;
foreach($currentkey as $key => $value):?>
<tr>
    <td><?= $key ?>
    <td><?= $value ?>
</style>
    <td><?= ctype_alpha($value) ? 'oui':'non'; ?></td>
    <td><?= ctype_digit($value) ? 'oui':'non'; ?></td>
    <td><?= filter_input(INPUT_POST,$key,FILTER_VALIDATE_EMAIL) ? "oui":"non"; ?></td>
    <td><?= filter_input(INPUT_POST,$key,FILTER_VALIDATE_INT) ? "oui":"non"; ?></td>
    <td><?= filter_input(INPUT_POST,$key,FILTER_VALIDATE_FLOAT) ? "oui":"non"; ?></td>
    <td>
            <?php
             if(filter_input(INPUT_POST,$key,FILTER_VALIDATE_EMAIL))
             {
                if(preg_match("/com|nze|fr$/",$value,$tm)){
               
               echo "email autorisé";
                }
            else {
                echo "email non autorisé";
                }
            }
            ?>
        </td>
    </tr>
</tr>
<?php
endforeach;
?>
</table>
<?php
// Vérification de l'entrée POST PORTES
$errorfound = [];
$validationok=0;
$validationcamion=0;
if (empty($_POST['portes'])){
    array_push ($errorfound, "Merci de renseigner le nombre de portes du véhicule.");
}
else if (!ctype_digit($_POST['portes'])){
    array_push ($errorfound, "Le nombre de portes n'est pas une valeur numérique.");
}
else {
    $portes = $_POST['portes'];
    $validationok++;
}
// Vérification de l'entrée POST ROUES
if (empty($_POST['roues'])){
    array_push ($errorfound, "Merci de renseigner le nombre de roues du véhicule.");
}
else if (!ctype_digit($_POST['roues'])){
    array_push ($errorfound, "Le nombre de roues n'est pas une valeur numérique.");
}
else {
    $roues = $_POST['roues'];
    $validationok++;
}
// Vérification de l'entrée POST MARQUE
if (empty($_POST['marque'])){
    array_push ($errorfound, "Merci de renseigner la marque du véhicule.");
}
else if (!ctype_alpha($_POST['marque'])){
    array_push ($errorfound, "La marque n'est pas une valeur alphabétique.");
}
else {
    $marque = $_POST['marque'];
    $validationok++;
}
// Vérification de l'entrée POST MODELE
if (empty($_POST['modele'])){
    array_push ($errorfound, "Merci de renseigner le modèle du véhicule.");
}
else if (!ctype_alnum($_POST['modele'])){
    array_push ($errorfound, "Le modèle n'est pas une valeur alphanumérique.");
}
else {
    $modele = $_POST['modele'];
    $validationok++;
}
// Vérification de l'entrée POST IMMATRICULATION
if (empty($_POST['immatriculation'])){
    array_push ($errorfound, "Merci de renseigner l'immatriculation du véhicule.");
}
else if (!ctype_alnum($_POST['immatriculation'])){
    array_push ($errorfound, "L'immatriculation n'est pas une valeur alphanumérique, merci de retirer les tirets.");
}
else {
    $immatriculation = $_POST['immatriculation'];
    $validationok++;
}
// Vérification de l'entrée POST COULEUR
if (empty($_POST['couleur'])){
    array_push ($errorfound, "Merci de renseigner la couleur du véhicule.");
}
else if (!ctype_alpha($_POST['couleur'])){
    array_push ($errorfound, "La couleur n'est pas une valeur alphabétique.");
}
else {
    $couleur = $_POST['couleur'];
    $validationok++;
}
// Vérification de l'entrée POST TYPE
if (empty($_POST['type'])){
    array_push ($errorfound, "Merci de renseigner le type du véhicule.");
}
else {
    $type = $_POST['type'];
    $validationok++;
}
// Vérification de l'entrée POST TONNAGE
if (empty($_POST['tonnage']) && $_POST['type'] == "Camion"){
    array_push ($errorfound, "Merci de renseigner le tonnage du camion.");
}
else if (!ctype_digit($_POST['tonnage']) && $_POST['type'] == "Camion"){
    array_push ($errorfound, "Le tonnage du camion n'est pas une valeur numérique.");
}
else {
    $tonnage = $_POST['tonnage'];
    $validationcamionok++;
}
// Vérification de l'entrée POST REMORQUELONGUEUR
if (empty($_POST['remorquelongueur']) && $_POST['type'] == "Camion"){
    array_push ($errorfound, "Merci de renseigner la longueur de la remorque.");
}
else if (!ctype_digit($_POST['remorquelongueur']) && $_POST['type'] == "Camion"){
    array_push ($errorfound, "La longueur de la remorque n'est pas une valeur numérique.");
}
else {
    $remorquelongueur = $_POST['remorquelongueur'];
    $validationcamionok++;
}
// Edition du fichier vehicule.csv
echo "<div style='text-align:center;'>";
if ($validationok == 7 && $_POST['type'] == "Voiture"){
    $temporaire = [$marque,$modele,$roues,$portes,$couleur,$immatriculation,$type];
    var_dump($temporaire);
    $file = fopen('./../datas/vehicule.csv', 'a');
    fputcsv($file, $temporaire, ";");
    fclose('./../datas/vehicule.csv');
    header("Location:secondepage.php");
}
else if ($validationok == 7 && $_POST['type'] == "Camion" && $validationcamion = 2){
    $temporaire = [$marque,$modele,$roues,$portes,$couleur,$immatriculation,$type,$tonnage,$remorquelongueur];
    var_dump($temporaire);
    $file = fopen('./../datas/vehicule.csv', 'a');
    fputcsv($file, $temporaire, ";");
    fclose('./../datas/vehicule.csv');
    header("Location:secondepage.php");
}
else {
    foreach ($errorfound as $key => $value){
        if ($value !== 0){
        echo $value . "<br/>";
        }
    }
}
echo "<a href='formulaire.php' class='link'>Retour au formulaire</a><br/>";
echo "<a href='index.php'class='link'>Retour à l'index</a>";
echo "</div>";
?>
</div>
</body>
</html>