<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" media="screen" type="text/css" href="css/style.css" />
    <title>Formulaire</title>
</head>
<body class= "formulaire">
  <div class="center global">
  <form action="validation.php" method="post" enctype="multipart/form-data">
    <div>
      <label for="Marque">Renseignez la marque du véhicule: <br/></label>
      <input type="text" id="marque" name="marque">
    </div>
    <div>
        <label for="Modele">Renseignez le modèle du véhicule: <br/></label>
        <input type="text" id="modele" name="modele">
    </div>
      <div>
          <label for="Portes">Renseignez le nombre de portes du véhicule: <br/></label>
          <input type="text" id="portes" name="portes">
      </div>
      <div>
          <label for="Roues">Renseignez le nombre de roues du véhicule: <br/></label>
          <input type="text" id="roues" name="roues">
      </div>
      <div>
          <label for="Couleur">Renseignez la couleur du véhicule: <br/></label>
          <input type="text" id="couleur" name="couleur">
      </div>
      <div>
          <label for="Immatriculation">Renseignez l'immatriculation (SANS LES -): <br/></label>
          <input type="text" id="immatriculation" name="immatriculation">
      </div>
      <div>
          <label for="type">Choississez le type du véhicule</label><br />
          <select name="type" id="type">
              <option value="">-------</option>
              <option value="Voiture">Voiture</option>
              <option value="Camion">Camion</option>
          </select>
          </label>
      </div>
      <div>
          <p>Camion uniquement</p>
      </div>
      <div>
          <label for="remorquelongueur">Renseignez la longueur de la remorque: <br/></label>
          <input type="text" id="remorquelongueur" name="remorquelongueur">
      </div>
      <div>
          <label for="tonnage">Renseignez le tonnage du camion: <br/></label>
          <input type="text" id="tonnage" name="tonnage">
      </div>
    <div class="button">
      <button type="submit">Envoyer</button>
    </div>  
  </form>
</div>
</body>
</html>