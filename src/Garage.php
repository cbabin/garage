<?php
Class Garage {
    public function expertise(Vehicule $Vehicule){
        echo  get_class($Vehicule) . " de marque " . $Vehicule->marque() . " va dans le garage. </br>";
        $Vehicule->demarrer()->avancer()->freiner()->arreter();
        $moteur = $Vehicule->moteurstatut();
        if ($moteur !== true){
            echo "le vehicule est certifié <br/>";
            $Vehicule->setcontroletechnique("CT-OK");
        }
        else {
            echo "le vehicule n'a pas pu être certifié <br/>";
            $Vehicule->setcontroletechnique("NON CERTIFIE");
        }
        return $Vehicule;
    }
}
?>